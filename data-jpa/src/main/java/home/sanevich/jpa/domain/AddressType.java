package home.sanevich.jpa.domain;

public enum AddressType {
    SHIPPING, BILLING
}
