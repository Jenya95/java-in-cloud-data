package home.sanevich;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Component
@RequiredArgsConstructor
public class JdbcCLR implements CommandLineRunner {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... args) {

        List<Object[]> userRecords = Stream.of(
                "Michael Hunger michael@examle.com",
                "Evgeny Sanevich sanevichj@gmail.com")
                .map(r -> r.split(" "))
                .collect(Collectors.toList());

        jdbcTemplate.batchUpdate("INSERT INTO user(first_name, last_name, email) VALUES (?,?,?)", userRecords);

        RowMapper<User> userRowMapper = ((rs, rowNum) ->
                new User(rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("email")));

        List<User> users = jdbcTemplate.query("SELECT id, first_name, last_name, email from user", userRowMapper);
        users.forEach(user -> log.info("{}", user));
    }
}
