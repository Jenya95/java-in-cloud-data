package home.sanevich.jpa.test;

import com.mysql.cj.exceptions.AssertionFailedException;
import home.sanevich.jpa.JpaApp;
import home.sanevich.jpa.dao.CustomerRepository;
import home.sanevich.jpa.domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static home.sanevich.jpa.domain.CreditCardType.VISA;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JpaApp.class)
@ActiveProfiles("test")
public class AccountApplicationTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void customerTest() {
        Account account = new Account("12345");
        Customer customer = new Customer("Jane", "Doe", "jane.doe@gmail.com", account);
        CreditCard creditCard = new CreditCard("1234567890", VISA);
        customer.getAccount().getCreditCards().add(creditCard);

        String street1 = "1600 Pennsylvania Ave NW";
        Address address = new Address(street1, null, "DC", "Washington", "USA", AddressType.SHIPPING, 20500);
        customer.getAccount().getAddresses().add(address);

        customer = customerRepository.save(customer);

        Customer persistedResult = customerRepository.findById(customer.getId()).get();
        Assert.assertNotNull(persistedResult);
        Assert.assertNotNull(persistedResult.getAccount());
        Assert.assertNotNull(persistedResult.getCreatedAt());
        Assert.assertNotNull(persistedResult.getLastModified());

        Assert.assertTrue(persistedResult.getAccount().getAddresses().stream()
                .anyMatch(a -> a.getStreet1().equalsIgnoreCase(street1)));

        customerRepository.findByEmailContaining(customer.getEmail())
                .orElseThrow(() -> new AssertionFailedException(new RuntimeException("must be a record")));
    }
}
