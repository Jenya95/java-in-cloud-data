package home.sanevich.mongo;

import home.sanevich.mongo.domain.BaseEntity;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class BeforeSaveListener extends AbstractMongoEventListener<BaseEntity> {
    @Override
    public void onBeforeSave(BeforeSaveEvent<BaseEntity> event) {
        LocalDateTime timeStamp = LocalDateTime.now();

        if (event.getSource().getCreatedAt() == null) {
            event.getSource().setCreatedAt(timeStamp);
        }

        event.getSource().setLastModified(timeStamp);

        super.onBeforeSave(event);
    }
}
