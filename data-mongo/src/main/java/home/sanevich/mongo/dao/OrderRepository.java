package home.sanevich.mongo.dao;

import home.sanevich.mongo.domain.Order;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, String> {
}
