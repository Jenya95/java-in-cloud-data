package home.sanevich.jpa.domain;

public enum CreditCardType {
    VISA, MASTERCARD
}
