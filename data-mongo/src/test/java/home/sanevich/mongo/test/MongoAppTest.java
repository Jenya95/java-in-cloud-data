package home.sanevich.mongo.test;

import home.sanevich.jpa.domain.Address;
import home.sanevich.jpa.domain.AddressType;
import home.sanevich.mongo.MongoApp;
import home.sanevich.mongo.dao.InvoiceRepository;
import home.sanevich.mongo.dao.OrderRepository;
import home.sanevich.mongo.domain.Invoice;
import home.sanevich.mongo.domain.LineItem;
import home.sanevich.mongo.domain.Order;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoApp.class)
public class MongoAppTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Before
    @After
    public void reset() {
        orderRepository.deleteAll();
        invoiceRepository.deleteAll();
    }

    @Test
    public void orderTest() {
        Address address = new Address("1600 Pennsylvania Ave NW", null, "DC", "Washington", "USA", AddressType.SHIPPING, 20500);

        Order order = new Order("12345", address);

        order.addLineItem(new LineItem("Some Best T-Shirt", "SKU-24642", 1, 21.99, .06));
        order.addLineItem(new LineItem("Bosh T-Shirt", "SKU-34563", 3, 14.99, .06));
        order.addLineItem(new LineItem("Bigger VM T-Shirt", "SKU-12464", 4, 13.99, .06));
        order.addLineItem(new LineItem("cf push awesome T-Shirt", "SKU-64233", 2, 21.99, .06));

        order = orderRepository.save(order);

        Assert.assertNotNull(order);
        Assert.assertEquals(order.getLineItems().size(), 4);

        Assert.assertEquals(order.getLastModified(), order.getCreatedAt());
        order = orderRepository.save(order);
        Assert.assertNotEquals(order.getLastModified(), order.getCreatedAt());

        Address billingAddress = new Address("875 Howard St.", null, "CA", "San Franciso", "Untited States",
                AddressType.BILLING, 99413);

        String accountNumber = "99127363677";

        Invoice invoice = new Invoice(accountNumber, billingAddress);
        invoice.addOrder(order);
        invoice = invoiceRepository.save(invoice);

        Assert.assertEquals(invoice.getOrders().size(), 1);

        Assert.assertEquals(invoiceRepository.findByBillingAddress(billingAddress), invoice);
    }
}