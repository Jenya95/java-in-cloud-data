rootProject.name = "java-in-cloud-spring-data"
include(":jdbc-template")
include(":data-jpa")
include(":data-mongo")
include(":data-neo4j")