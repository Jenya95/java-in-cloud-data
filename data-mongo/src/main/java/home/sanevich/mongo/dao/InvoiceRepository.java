package home.sanevich.mongo.dao;

import home.sanevich.jpa.domain.Address;
import home.sanevich.mongo.domain.Invoice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, String> {
    Invoice findByBillingAddress(Address address);
}
