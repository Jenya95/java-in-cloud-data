package home.sanevich.mongo.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseEntity {
    private LocalDateTime lastModified;
    private LocalDateTime createdAt;
}
