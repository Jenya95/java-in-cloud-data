package home.sanevich;

import lombok.Value;

@Value
public class User {
    private final Long id;
    private final String firstName;
    private final String lastName;
    private final String email;
}
