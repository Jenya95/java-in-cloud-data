package home.sanevich.mongo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LineItem {
    private String name;
    private String productId;
    private Integer quantity;
    private Double price;
    private Double tax;
}
