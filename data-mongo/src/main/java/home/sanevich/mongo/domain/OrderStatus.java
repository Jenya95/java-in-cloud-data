package home.sanevich.mongo.domain;

public enum  OrderStatus {
    PENDING, CONFIRMED, SHIPPED, DELIVERED
}
