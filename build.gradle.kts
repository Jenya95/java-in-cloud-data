import org.gradle.api.JavaVersion.VERSION_1_8

plugins {
    java
}

buildscript {
    repositories {
        mavenCentral()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.1.2.RELEASE")
        classpath("io.freefair.gradle:lombok-plugin:3.1.4")
    }
}

subprojects {
    apply {
        plugin("org.springframework.boot")
        plugin("io.freefair.lombok")
    }
}

allprojects {
    apply {
        plugin("io.spring.dependency-management")
        plugin("java")
    }

    repositories {
        mavenCentral()
    }
}

group = "home.sanevich"
version = "0.0.1-SNAPSHOT"

configure<JavaPluginConvention> {
    sourceCompatibility = VERSION_1_8
    targetCompatibility = VERSION_1_8
}

project(":jdbc-template") {
    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-data-jpa")
        runtimeOnly("mysql:mysql-connector-java")
        testImplementation("org.springframework.boot:spring-boot-starter-test")
    }
}

project(":data-jpa") {
    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-data-jpa")
        runtimeOnly("mysql:mysql-connector-java")
        testCompile("com.h2database:h2")
        testImplementation("mysql:mysql-connector-java")
        testImplementation("org.springframework.boot:spring-boot-starter-test")
    }
}

project(":data-mongo") {
    sourceSets {
        main {
            compileClasspath += project(":data-jpa").sourceSets["main"].output
            runtimeClasspath += project(":data-jpa").sourceSets["main"].output
        }
        test {
            compileClasspath += project(":data-jpa").sourceSets["main"].output
            runtimeClasspath += project(":data-jpa").sourceSets["main"].output
        }
    }

    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
        implementation("org.springframework.boot:spring-boot-starter-data-jpa")
        runtimeOnly("mysql:mysql-connector-java")
        testImplementation("org.springframework.boot:spring-boot-starter-test")
    }
}

project(":data-neo4j") {
    tasks {
        "bootJar" {
            enabled = false
        }
    }

    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-data-neo4j")
    }

}