package home.sanevich.mongo.domain;

public enum InvoiceStatus {
    CREATED, SENT, PAID
}
