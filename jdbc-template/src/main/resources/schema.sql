USE home;
DROP TABLE IF EXISTS user;
CREATE TABLE user
(
  id         serial,
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  email      VARCHAR(255)
)